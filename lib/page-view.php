<div class="wrap" ng-app="segments">
    <a href="/wp-admin/admin.php?page=segments" class="button">&laquo; Back to Segments</a>

    <div ng-controller="SegmentsViewCtrl" ng-cloak>
        <div class="notice notice-info" ng-show="is_loading">
            <p><span class="spinner is-active" style="float:left;margin-top:0;"></span> Loading segment...</p>
        </div>

        <div class="segment" ng-if="!is_loading">
            <h1>{{ segment.title }}</h1>

            <div class="notice notice-warning" ng-if="segment.done===0">
                <p><span class="spinner is-active" style="float:left;margin-top:0;"></span> This segment is still being
                    processed...</p>
            </div>

            <div class="notice notice-success" ng-if="segment.done===1">
                <p>Active</p>
            </div>

            <p><strong>Rules</strong></p>

            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                <tr>
                    <td>Attribute</td>
                    <td>Operator</td>
                    <td>Value</td>
                </tr>
                </thead>

                <tbody>
                <tr ng-repeat="rule in segment.segment_rules">
                    <td>{{ rule.key }}</td>
                    <td>{{ rule.operator }}</td>
                    <td>{{ rule.value }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>