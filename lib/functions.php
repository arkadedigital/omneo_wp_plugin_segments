<?php namespace Omneo\Segments;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');

/**
 * Segments admin menu
 */
function menu()
{
    add_menu_page(
        'Segments',
        'Segments',
        'manage_options',
        'segments',
        __NAMESPACE__ . '\\page_list',
        'dashicons-editor-ol'
    );

    add_submenu_page(
        null,
        'Segments',
        'Segments',
        'manage_options',
        'segments-add',
        __NAMESPACE__ . '\\page_add'
    );

    add_submenu_page(
        null,
        'Segments',
        'Segments',
        'manage_options',
        'segments-view',
        __NAMESPACE__ . '\\page_view'
    );
}

add_action('admin_menu', __NAMESPACE__ . '\\menu');

// Segments list page
function page_list()
{
    // Load page
    require_once(__PATH() . '/lib/page-list.php');
}

// Segment add page
function page_add()
{
    // Load page
    require_once(__PATH() . '/lib/page-add.php');
}

// Segment view page
function page_view()
{
    // Load page
    require_once(__PATH() . '/lib/page-view.php');
}




/**
 * Load assets
 */
function load_assets()
{
    if(isset($_GET['page']) && in_array($_GET['page'], ['segments', 'segments-add', 'segments-view'])) {
        wp_enqueue_script('jquery-ui', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'jquery-ui.min.js');
        wp_enqueue_script('lodash', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'lodash.min.js');
        wp_enqueue_style('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.css');
        wp_enqueue_script('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.js');
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('angular-route', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-route.min.js');
        wp_enqueue_script('angular-slugify', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-slugify.js');
        wp_enqueue_script('angular-ui-sortable', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-ui-sortable.min.js');
        wp_enqueue_script('segments', plugins_url() . DS . 'omneo-segments' . DS . 'assets' . DS . 'main.js');
    }

    // Content item
    if((isset($_GET['post']) && get_post_type($_GET['post']) == 'content_items') || (isset($_GET['post_type']) && $_GET['post_type'] == 'content_items')) {
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('segments', plugins_url() . DS . 'omneo-segments' . DS . 'assets' . DS . 'content-item.js');
    }
}
load_assets();

function segments_send_request()
{
    $data = json_decode(stripslashes($_GET['data']), true);
    $response = Core\send_request($data);
    $json = json_encode($response);
    echo $json;
    exit;
}

add_action('wp_ajax_segments_send_request', __NAMESPACE__ . '\\segments_send_request');


// Setup metabox
function omneo_segments_post_meta_box_setup()
{
    add_meta_box(
        'content-item-segments',           // Unique ID
        esc_html__('Segments', 'segments'),   // Title
        __NAMESPACE__ . '\\content_item_segments',       // Callback function
        'content_items',                // Admin page (or post type)
        'side',                       // Context
        'default'                       // Priority
    );
}
add_action('load-post.php', __NAMESPACE__ . '\\omneo_segments_post_meta_box_setup');
add_action('load-post-new.php', __NAMESPACE__ . '\\omneo_segments_post_meta_box_setup');


/**
 * Rules view
 */
function content_item_segments()
{
    // Load metabox
    require_once(__PATH() . '/lib/content-item.php');
}