<div class="wrap" ng-app="segments">

    <h2>Add Segment</h2>

    <br>

    <div ng-controller="SegmentsAddCtrl" ng-cloak>
        <form name="rules_form" ng-submit="save_segment()">
            <div id="titlediv">
                <input type="text" id="title" name="title" placeholder="Enter title here" ng-model="segment.title" required>
            </div>

            <h3>Rules</h3>
            <table class="wp-list-table widefat striped posts">
                <thead>
                <tr ng-show="segment.rule_set.length === 0">
                    <th colspan="100%">No rules</th>
                </tr>
                <tr ng-show="segment.rule_set.length > 0">
                    <td>User Attribute</td>
                    <td>Operator</td>
                    <td>Value</td>
                    <td></td>
                </tr>
                </thead>

                <tbody ng-model="segment.rule_set" ui-sortable>
                <tr ng-repeat="rule in segment.rule_set">
                    <td>
                        <select name="user_attribute" ng-model="rule.key" required ng-if="!is_loading_attributes">
                            <option value="">Select attribute</option>
                            <option ng-repeat="attribute in user_attributes" value="{{ attribute }}">{{ attribute }}</option>
                        </select>

                        <span ng-if="is_loading_attributes">Loading attributes...</span>
                    </td>
                    <td>
                        <select name="operator" ng-model="rule.operator" required>
                            <option value="">Select operator</option>
                            <option ng-repeat="operator in operators" value="{{ operator.value }}">
                                {{ operator.title }}
                            </option>
                        </select>
                    </td>
                    <td><input type="text" name="value" ng-model="rule.value" placeholder="Enter a value" required></td>
                    <td>
                        <div class="row-actions visible">
                            <button type="button" class="button button-primary button-large"
                                    ng-click="remove_rule(rule)"
                                ng-hide="$index===0">
                                Remove
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>

                <tfoot>
                <tr>
                    <td colspan="5">
                        <button type="button" class="button button-primary button-large" ng-click="add_rule()">
                            Add Rule
                        </button>
                    </td>
                </tr>
                </tfoot>

            </table>
            <br>

            <button class="button button-primary button-large" ng-disabled="saving || !rules_form.$valid">
                Save Segment
            </button>
            <span class="spinner" ng-class="{'is-active': saving}" style="float:none;"></span>

        </form>
    </div>
</div>