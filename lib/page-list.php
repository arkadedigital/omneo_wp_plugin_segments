<div class="wrap" ng-app="segments">
    <h1>
        Segments
        <a href="/wp-admin/admin.php?page=segments-add" class="page-title-action">Add New Segment</a>
    </h1>
    <br>

    <div ng-controller="SegmentsListCtrl" ng-cloak>
        <div class="notice notice-info" ng-show="is_loading">
            <p><span class="spinner is-active" style="float:left;margin-top:0;"></span> Loading segments...</p>
        </div>


        <table class="wp-list-table widefat striped posts" ng-if="!is_loading && segments.length > 0">
            <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Status</td>
                <td></td>
            </tr>
            </thead>

            <tbody>
            <tr ng-repeat="segment in segments">
                <td>{{ segment.id }}</td>
                <td>{{ segment.title }}</td>
                <td>
                    <div ng-if="segment.done!==1"><span class="spinner is-active" style="float:none;margin:0;"></span> Segment not ready yet</div>
                    <div ng-if="segment.done===1">Active</div>
                </td>
                <td>
                    <div>
                        <a href="/wp-admin/admin.php?page=segments-view&id={{ segment.id }}" class="button"
                           ng-click="view_segment(segment)">
                            View
                        </a>
                        <button type="button" class="button"
                                ng-click="delete_segment(segment)"
                                >
                            Delete
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="notice notice-info" ng-if="!is_loading && segments.length === 0">
            <p>No segments</p>
        </div>

    </div>
</div>