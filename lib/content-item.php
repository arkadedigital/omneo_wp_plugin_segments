<div id="ng-segments">
    <?php
    global $CONTENT_TYPE_LIST;
    $content_type = $CONTENT_TYPE_LIST[get_field('content_type', $_GET['post'])];
    $content_item_id = $content_type ? \Omneo\Content\get_content_item_id(get_field('omneo_id', $_GET['post']), $content_type) : '';
    ?>

    <?php
    $guest_only = get_field('guest_only', $_GET['post']);
    $is_public = get_field('is_public', $_GET['post']);
    $segment_type = 'is_public';

    if(is_null($guest_only) || is_null($is_public)) {
        $segment_type = 'is_public';
    } elseif ($guest_only == 1 && $is_public == 0) {
        $segment_type = 'guest_only';
    } elseif ($guest_only == 0 && $is_public == 0) {
        $segment_type = 'custom';
    }
    ?>
    <div ng-controller="SegmentsCtrl" ng-cloak
         ng-init="content_item_id='<?php echo $content_item_id ?>'; content_type='<?php echo $content_type; ?>'; segment_type='<?php echo $segment_type; ?>'">

        <p>
            <label><input type="radio" name="segment_type" value="is_public" ng-click="set_segment_type('is_public')" ng-model="segment_type"> Public</label><br>
            <label><input type="radio" name="segment_type" value="guest_only" ng-click="set_segment_type('guest_only')" ng-model="segment_type"> Guest
                Only</label><br>
            <label><input type="radio" name="segment_type" value="custom" ng-click="set_segment_type('custom')" ng-model="segment_type"> Custom
                Segments</label>
        </p>

        <p class="description">
            <span ng-if="segment_type==='guest_only'">Only guests will see this content</span>
            <span ng-if="segment_type==='is_public'">Everyone will be able to see this content</span>
            <span ng-if="segment_type==='custom'">You must select at least one segment. This content will visible to users in each of the segments listed below.</span>
        </p>

        <br>

        <div ng-if="is_loaded && segment_type==='custom'">
            <select ng-model="selected_segment" ng-options="segment.title for segment in segments track by segment.id" ng-change="add_segment(selected_segment)" style="width:100%;">
                <option value="">-- Select a segment to add --</option>
            </select>

            <table>
                <tr ng-repeat="segment in selected_segments" ng-show="segment.action !== 'delete'">
                    <td>#{{ segment.segment_id }}: {{ segment.title }}</td>
                    <td>
                        <div class="dashicons dashicons-dismiss" ng-click="delete_segment(segment)"></div>
                        <input type="hidden" name="segments[{{ $index }}][action]" value="{{ segment.action }}">
                        <input type="hidden" name="segments[{{ $index }}][id]" value="{{ segment.id }}"
                               ng-disabled="!segment.action || content_type==='lookbooks' || content_type==='gallery'">
                        <input type="hidden" name="segments[{{ $index }}][id]" value="{{ segment.segment_id }}"
                               ng-disabled="content_type!=='lookbooks' && content_type!=='gallery'">
                    </td>
                </tr>
            </table>
        </div>

        <div ng-if="segments.length===0 && is_loaded">No segments</div>
        <div ng-if="!is_loaded && segment_type==='custom'"><span class="spinner is-active" style="float:none;margin-top:0;"></span>
            Loading segments...
        </div>
    </div>
</div>


