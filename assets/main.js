var app = angular.module('segments', ['ngRoute', 'ui.sortable', 'slugifier']);

app.constant('Operators', [
    {value: 'eq', title: 'Equal'},
    {value: 'neq', title: 'Not equal'},
    {value: 'lt', title: 'Less than'},
    {value: 'lte', title: 'Less than or equal to'},
    {value: 'gt', title: 'Greater than'},
    {value: 'gte', title: 'Greater than or equal to'},
    {value: 'like', title: 'like'},
    {value: 'ilike', title: 'like (case insensitive)'}
]);

/**
 * List controller
 */
app.controller('SegmentsListCtrl', function ($scope, segmentsService) {
    $scope.is_loading = true;

    // Get segments
    segmentsService.getSegments().then(function (data) {
        $scope.is_loading = false;
        $scope.segments = data;
    });

    $scope.delete_segment = function (segment) {
        if (confirm('Are you sure you want to delete the segment #' + segment.id + ': ' + segment.title + '')) {
            segmentsService.deleteSegment({id: segment.id}).then(function () {
                toastr.success('Segment #' + segment.id + ' deleted');
                _.remove($scope.segments, function (s) {
                    return s.id === segment.id;
                });
            }, function (reason) {
                toastr.error(reason);
            });
        }
    };
});

/**
 * View controller
 */
app.controller('SegmentsViewCtrl', function ($scope, $window, $interval, segmentsService) {
    $scope.is_loading = true;

    // Get id from query string
    _.each($window.location.search.replace('?', '').split('&'), function (str) {
        var param = str.split('=');
        if (param[0] == 'id') {
            $scope.segment_id = isNaN(parseInt(param[1])) ? 0 : param[1];
        }
    });

    segmentsService.getSegment({id: $scope.segment_id}).then(function (data) {
        $scope.is_loading = false;
        $scope.segment = data;

        // If segment still processing
        if ($scope.segment.done === 0) {
            // Check every 10 seconds
            var interval_promise = $interval(function () {
                if ($scope.segment.done === 1) {
                    $interval.cancel(interval_promise);
                }
            }, 10000);
        }
    }, function (reason) {
        toastr.error(reason);
    });
});

/**
 * Add controller
 */
app.controller('SegmentsAddCtrl', function ($scope, $window, Slug, segmentsService, userAttributesService, Operators) {

    $scope.is_loading_attributes = true;

    userAttributesService.getUserAttributes().then(function (data) {
        $scope.user_attributes = data;
        $scope.is_loading_attributes = false;
    });

    $scope.operators = Operators;
    $scope.segment = {
        rule_set: [{}]
    };

    $scope.add_rule = function () {
        $scope.segment.rule_set.push({});
    };

    $scope.remove_rule = function (rule) {
        _.remove($scope.segment.rule_set, rule);
    };

    $scope.save_segment = function () {
        $scope.saving = true;
        var post_data = {
            title: $scope.segment.title,
            rule_set: angular.toJson($scope.segment.rule_set) // Convert rules to json string
        };

        segmentsService.postSegment(post_data).then(function (data) {
            toastr.success('Segment saved');
            $window.location.href = '/wp-admin/admin.php?page=segments-view&id=' + data.id;
        }, function (reason) {
            toastr.error(reason);
        }).finally(function () {
            $scope.saving = false;
        });
    };
});


/**
 * User Attributes service
 */
app.factory('userAttributesService', function ($http) {
    return {
        getUserAttributes: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'userattributekeys'
                    }
                }
            }).then(function (response) {
                return response.data.data;
            });
        }
    };
});

/**
 * Segments service
 */
app.factory('segmentsService', function ($http, $q) {
    return {
        /**
         * Get segments
         */
        getSegments: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'segments'
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        /**
         * Get single segment
         * @param args
         * @returns {HttpPromise}
         */
        getSegment: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'segments/' + args.id
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        /**
         * Create segment
         * @param args
         * @returns {HttpPromise}
         */
        postSegment: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'post',
                        api_request: 'segments',
                        data: args
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        /**
         * Delete segment
         * @param args
         * @returns {HttpPromise}
         */
        deleteSegment: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'delete',
                        api_request: 'segments/' + args.id,
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        }
    };
});