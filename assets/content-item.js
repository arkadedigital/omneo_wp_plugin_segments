angular.module('segments', []);
angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById('ng-segments'), ['segments']);
});

angular.module('segments').controller('SegmentsCtrl', function ($scope, segmentsService, $timeout) {
    $scope.segments = [];
    $scope.selected_segments = [];

    $scope.segment_types = [
        {id: 'public', label: 'Public'},
        {id: 'guest', label: 'Guest Only'},
        {id: 'custom', label: 'Custom segments'}
    ];


    // Get segments
    segmentsService.getSegments().then(function(data) {
        $scope.segments = data;
    });

    // Wait for $scope.content_item_id to initialize
    $timeout(function () {
        // Get content item segments
        if ($scope.content_item_id) {
            segmentsService.getContentSegments({content_item_id: $scope.content_item_id}).then(function (data) {
                // Set title
                _.each(data, function (content_item_segment) {
                    var segment = _.find($scope.segments, function (v) {
                        return v.id == content_item_segment.segment_id;
                    });

                    if(segment) {
                        content_item_segment.title = segment.title;

                        // Default action to 'add' if lookbook or gallery content type
                        if($scope.content_type === 'lookbooks' || $scope.content_type === 'gallery') {
                            content_item_segment.action = 'add';
                        }
                    } else {
                        content_item_segment.title = 'Segment does not exist';
                    }
                });

                $scope.selected_segments = data;
                $scope.is_loaded = true;
            });
        } else {
            $scope.is_loaded = true;
        }
    });

    $scope.add_segment = function (segment) {
        // If added already, don't add again
        var segment_exists = _.find($scope.selected_segments, function (selected_segment) {
            return selected_segment.segment_id == segment.id;
        });
        if (segment_exists) {
            segment_exists.action = '';
            return false;
        }

        segment.action = 'add';
        segment.segment_id = segment.id;
        $scope.selected_segments.push(segment);
    };

    $scope.delete_segment = function (segment) {
        if (segment.action !== 'add') {
            segment.action = 'delete';
        } else {
            $scope.selected_segments = _.without($scope.selected_segments, segment);
        }
    };

    $scope.set_segment_type = function(segment_type) {
        var elem_is_public = jQuery('#acf-field_574680ef48936');
        var elem_guest_only = jQuery('#acf-field_57467b5d48935');

        switch(segment_type) {
            case 'guest_only':
                jQuery(elem_guest_only).val(1);
                jQuery(elem_is_public).val(0);
                break;

            case 'is_public':
                jQuery(elem_guest_only).val(0);
                jQuery(elem_is_public).val(1);
                break;

            case 'custom':
                jQuery(elem_guest_only).val(0);
                jQuery(elem_is_public).val(0);
                break;

            default:
                jQuery(elem_guest_only).val(0);
                jQuery(elem_is_public).val(1);

        }
    };
});

/**
 * Segments service
 */
angular.module('segments').factory('segmentsService', function ($http, $q) {
    return {
        /**
         * Get content item segments
         * @returns {*}
         */
        getContentSegments: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'contentsegments',
                        data: args
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        /**
         * Get segments
         * @returns {*}
         */
        getSegments: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'segments'
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        }
    };
});